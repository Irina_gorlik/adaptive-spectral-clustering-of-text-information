using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Results
{
    public class Results
    {
        public  double[][] Data; // data set
        public  int[] Clusters; // cluster assignment

        // constructor
        public Results(double[][] DataSet, int K)
        {
            Data =  DataSet;
            Clusters = new int[K];
            for (int i = 0; i < K; i++) // reset Clusters
                Clusters[i] = -1;

        }

        public void LoadClusters(int[] Res)
        {  
            Clusters = Res;
        }// end LoadClusters





    }// end calss Results.

    
}// end namespace
