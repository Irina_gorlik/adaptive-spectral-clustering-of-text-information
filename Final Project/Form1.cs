﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using StopWordsHandler;
using System.Windows.Forms.DataVisualization.Charting;
using System.Security;
using Microsoft.VisualBasic;
using CramersV;
using System.Threading;
using System.Drawing;


namespace Final_Project
{
    public partial class Form1 : Form
    {

        private string[] myPath = new string[100];     // stores listBox file path.
        private int Numoftxt=0;                       // counts number of text in listBox1.
        private string selectedItem = "";
        private string typeofInput = "doc";          // "doc" : defualt type is document, or matrix "mat".
        private string typeofCluster = "PAM";        // "pam" : defualt type is pam clustering, or "means" as in k-means.
        private string typeofResult = "hist";        // "hist" : defualt type is histrogram, or "ass" as assosiative table.
        private double brentsTolerance = 1e-6;       // defualt tolerance for brent's method.
        private int NumofClusters = 3;                 // defualt number of clusters.
        private ListBox listBox2;                   // actual listbox with full files path names.
        public double Sigma = 0.1;                   // initial scalling factor.        
        public double xmin;                     // brents method ref xmin;
        public int loopEndBy = 0;                // end by epsilon = 0 , end by runs = 1.
        public double NewSigma =-1;         
        public double epsilon = 0.001;          
        public int runs = 10;
        public int stoploop = 0;
        public List<Chart> list = new List<Chart>();
        public string[] fileName = new string[100];
        public double[][] assoTable;
        public double CramersVal = 0;
        public int div = 10;                     // all the books will be divides into "div" parts.
        public int[] Error = new int[10];
        public double[,] specData;
        public double MaxSigma = 0;
        public double MaxCramer = 0;
        public string ButtonPress = "off";      // when start algorithm button is pressed "ButtonPress = "on" ".
        public double[][] SaveBow = null;       // here we save the bow and then we can put inside a text file to save
        public int SaveSafe = 0;
        public double[][] BOW = null;
        public int bowFromFile = 0;
        public int Counter = 0;
        public int StopBy = 0;
        public int r = 0;  // number of iterations.



        public Form1()
        {
           //Control.CheckForIllegalCrossThreadCalls = false;
 
            InitializeComponent();
            progressBar1.Step = 100 / runs;
            this.MaximizeBox = false;
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {


        }

        private void button1_Click(object sender, EventArgs e) // add new document button event
        {
            
           openFileDialog1 = new OpenFileDialog();
           openFileDialog1.FileOk += new CancelEventHandler(OnFileOpenOK);
           openFileDialog1.Multiselect = true;

           openFileDialog1.Filter = "Text documents (*.txt)|*.txt";
           openFileDialog1.FilterIndex = 1;
           openFileDialog1.ShowDialog();
    

        }
        private void OnFileOpenOK(Object sender, CancelEventArgs e) // button ok in file browser event
        {
            if (bowFromFile == 0)
            {
                // Read the files
                foreach (String fileName in openFileDialog1.FileNames)
                {
                    // Create a PictureBox.
                    try
                    {
                        myPath[Numoftxt++] = fileName; // saves path for later use. (not really using it yet)
                        listBox1.Items.Add(Path.GetFileName(fileName)); // add only name of file to the listBox1
                        string[] divFileName = new string[div];
                        divFileName = divideText(fileName);
                        if (Error[0] == 0)
                        {
                            foreach (String fileName1 in divFileName)
                                Functions.Start().mySettings.AddToListbox(fileName1); // old way : Functions.Start().mySettings.AddToListbox(fileName);
                        }// end if no error
                        else if (Error[0] == 1) // file not found
                            MessageBox.Show("Can't open file");
                        else if (Error[0] == 2) // text to short to divide
                            MessageBox.Show("Can't divide book to " + div + " parts, book is to short.");

                        listBox2 = Functions.Start().mySettings.getFullPath();
                        Error[0] = 0; //reset
                        SaveSafe = 1;
                    }
                    catch (SecurityException ex)
                    {
                        // The user lacks appropriate permissions to read files, discover paths, etc.
                        MessageBox.Show("Security error. Please contact your administrator for details.\n\n" +
                              "Error message: " + ex.Message + "\n\n" +
                              "Details (send to Support):\n\n" + ex.StackTrace
                         );
                    }
                    catch (Exception ex)
                    {
                        // Could not load the image - probably related to Windows file system permissions.
                        MessageBox.Show("Cannot display the image: " + fileName.Substring(fileName.LastIndexOf('\\'))
                            + ". You may not have permission to read the file, or " +
                            "it may be corrupt.\n\nReported error: " + ex.Message);
                    }
                }
            }// end if bowfromfile == 0 : not from text file
            else // bow from text file
            {
              string fileName2 = openFileDialog1.FileName; 
              myPath[Numoftxt++] = fileName2; 
              listBox1.Items.Add(Path.GetFileName(fileName2));
              Functions.Start().mySettings.AddToListbox(fileName2);
              
           //   listBox2 = Functions.Start().mySettings.getFullPath();
                  
            }// end else

         
            
            
  
        }// end event


        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
       
        }

        private void button4_ClientSizeChanged(object sender, EventArgs e)
        {

        }



        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            
        }

        delegate void CallPerformStep(ProgressBar myProgressBar);

        private void ActuallyPerformStep(ProgressBar myProgressBar)
        {
            if (progressBar1.InvokeRequired)
            {
                CallPerformStep del = ActuallyPerformStep;
                myProgressBar.Invoke(del, new object[] { myProgressBar });
                return;
            }

            myProgressBar.PerformStep();
        }
        delegate void updateLabelTextDelegate(string newText, Label myLabel);
        private void updateLabelText(string newText, Label myLabel)
        {
            if (myLabel.InvokeRequired)
            {
                // this is worker thread
                updateLabelTextDelegate del = new updateLabelTextDelegate(updateLabelText);
                myLabel.Invoke(del, new object[] { newText, myLabel });
            }
            else
            {
                // this is UI thread
                myLabel.Text = newText;
            }
        }
        



        public void ThreadJob()
        {
            
            if (validateOptions()) // validate inputs before starting the algorithm. then start algorithms.
            {

               ;

                if (bowFromFile == 0)
                {
                    // BoW            
                    BagOfWords.BagOfWords bow = new BagOfWords.BagOfWords(Functions.Start().mySettings.getFullPath());
                    bow.StartBagOfWords();
                    BOW = bow.GetNormalizeBowMatrix();
                }
                else
                {
                    string asFilesPath;
                    asFilesPath = Functions.Start().mySettings.getItemfromList(0);
                    asFilesPath = asFilesPath.Replace("\\", "\\\\");
                    //listBox2.Items.RemoveAt(0);
                    string[] lines = System.IO.File.ReadAllLines(asFilesPath);
                    double[][] TBOW = new double[Convert.ToInt32(lines[(lines.Length) - 1])][];
                    // for each line
                    int i;
                    for (i = 0; lines[i] != "*" && i < lines.Length-1; i++)
                    {
                        string[] words = lines[i].Split(' ');
                            TBOW[i] = new double[words.Length - 1];
                            for (int j = 0; j < words.Length - 1; j++)
                            {
                               TBOW[i][j] = Convert.ToDouble(words[j]);
                            } // inner for
                       
                    } // for lines
                    BOW = TBOW;
                    listBox2 = new ListBox();
                    i = i + 2;
                    div = Convert.ToInt32(lines[i]);
                    for (i = i +1; i < lines.Length-1; i++)
                    {
                        listBox2.Items.Add(lines[i]);
                    } // for lines



                
                }
                int n = listBox2.Items.Count; // number of texts total;

                string[] BookNames = new string[n]; // will put book names here.
                string[] tableRow = new string[NumofClusters];
                // save to SaveBow array so the user can save later into text file
                SaveBow = BOW;
                SaveSafe = 1;

                


                // Reset results
                Results.Results Res = new Results.Results(BOW, NumofClusters);
                int[] ClusterResults = new int[NumofClusters];
                
                
                //loop begins from here.
                
                while (stoploop == 0) 
                {
     
                
                /*  NGW Algorithm:
                 *  Ng, A. Y., M. I. Jordan, and Y. Weiss Spectral Clustering Alorithm
                 *  input: bag of words matrix, number of clusters(number of books), pam/k-means.
                 *  output: clustering result matrix.
                 */
                    SpectralClustering.SpectralClustering spectral = new SpectralClustering.SpectralClustering(BOW, listBox2.Items.Count, typeofCluster, Sigma, NumofClusters);// SpectralClustering.SpectralClustering spectral = new SpectralClustering.SpectralClustering(bow.GetBowMatrix(), listBox1.Items.Count , typeofCluster, Sigma);
                 
               
                    ClusterResults = spectral.StartClustering();
                    Res.LoadClusters(ClusterResults); // part 6 at NJW algorithm
                    // reset list
               
                    // build Chart reuslt
                    if (r == 0) // if first run create new list
                        for (int i = 0, k = 1; i < listBox2.Items.Count; i++, k++)
                            list.Add(new Chart(listBox2.Items[i].ToString(), ClusterResults[i], "Cluster " + k, getVector(BOW, i)));

                    else // not first run just need to change list 
                        for (int i = 0, k = 1; i < listBox2.Items.Count; i++, k++)
                            list[i].setCluster(ClusterResults[i]);
               
                    
                /*
                 *  Brent's Method                   
                 */

                 Brent.Brent brent = new Brent.Brent(0,0.5, 1, brentsTolerance, ref xmin, NumofClusters, listBox2.Items.Count, list);
                 double ret = brent.brentMethod(ref xmin); // returns function value, and xmin = the actual minimun point.
                 NewSigma = xmin;

                 r++;  // increment number of  iterations.

                 switch (StopBy)
                 {
                     case 1:
                         {
                             if (Math.Abs(Sigma - NewSigma) < epsilon)
                                 stoploop = 1;
                             else
                                 Sigma = NewSigma;
                             break;
                         }// end case 1
                     case 2:
                         {
                             if (Math.Abs(Sigma - NewSigma) < epsilon || r >= runs)
                                 stoploop = 1;
                             else
                                 Sigma = NewSigma;
                             break;
                         }// end case 2
                       
                     default:
                         {
                             if (r >= runs)
                                 stoploop = 1;
                             else
                                 Sigma = NewSigma;
                           
                             break;
                         }// end case 0 (default)


                 }// end switch

                     ActuallyPerformStep(progressBar1);
    

                 } // end loop
                // loop ends here
                stoploop = 0;


                // reset list
                int size2 = list.Count;
                for (int i = 0; i < size2; i++)
                    list.RemoveAt(0);






                // build Chart reuslt
                for (int i = 0, k = 1; i < listBox2.Items.Count; i++, k++)
                {
                    list.Add(new Chart(listBox2.Items[i].ToString(), ClusterResults[i], "Cluster " + k, getVector(BOW, i)));


                }// end for
              //  setGridtabe(list); // set grid values in table & draw graph


                /*
                 *  Present Results
                 */


                updateLabelText(Convert.ToString(Math.Round(Sigma, 3)), label25); //label25.Text = Convert.ToString(Math.Round(Sigma, 3) );// scalling factor

                updateLabelText(Convert.ToString(NumofClusters), label27);//label27.Text = Convert.ToString(NumofClusters);// number of clusters

                updateLabelText(Convert.ToString(listBox2.Items.Count), label29);//label29.Text = Convert.ToString(listBox2.Items.Count);// number of total texts

                updateLabelText(typeofCluster, label30);//label30.Text = typeofCluster;// clustering algorithm

                updateLabelText(Convert.ToString(r), label31); // label31.Text = Convert.ToString(brentsTolerance);//brent's tolerance
                 
                
                setGridtabe(list); // set grid values in table & draw graph

                   
                /*
                 * Reset all for next use
                 */


                  // reset text from listbox
                r = 0;
                this.Invoke((MethodInvoker)delegate { listBox1.Items.Clear(); });   //   listBox1.Items.Clear();                             // gui clear.
                
                Functions.Start().mySettings.ClearAllDocuments();   // real listbox clear.
                    Array.Clear(fileName, 0, fileName.Length);
                    Array.Clear(myPath, 0, myPath.Length);
                    Numoftxt = 0;
                // reset start button
                    button4.Image = ((System.Drawing.Image)(Properties.Resources.start11)); // change image at click

                // reset process bar


                    this.Invoke((MethodInvoker)delegate { progressBar1.Value = 0; });   //  progressBar1.Value = 0;
                  
                    
                
                    MessageBox.Show("The Process is Complete");


                    this.Invoke((MethodInvoker)delegate { tabControl1.SelectedTab = tabPage3; });   //  tabControl1.SelectedTab = tabPage3;
                  
                    




            //      drawGraph(list); // draw chart
            }// end if valid

            ButtonPress = "off"; // reset button 
        }// end start button
 

        


        private void button4_Click_1(object sender, EventArgs e) // start algorithm button event
        {

            button4.Image = ((System.Drawing.Image)(Properties.Resources.start22)); // change image at click
          //  timer1.Enabled = true; // start timer 


            // set datagridview
            ResetDatagridview(dataGridView1);
            ResetDatagridview(dataGridView2);
            for (int i = 0; i < NumofClusters; i++) // add the amount of cluster into column in gridview1.
            {
                DataGridViewColumn col = new DataGridViewColumn();
                DataGridViewCell cell = new DataGridViewTextBoxCell();
                col.CellTemplate = cell;
                col.HeaderText = Convert.ToString(i + 1);
                col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                col.Name = Convert.ToString(i + 1);
                col.Visible = true;
                dataGridView1.Columns.Add(col);
                dataGridView1.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }// end for: adding columns to the gridview.
            DataGridViewColumn col1 = new DataGridViewColumn();
            DataGridViewCell cell1 = new DataGridViewTextBoxCell();
            col1.CellTemplate = cell1;
            col1.HeaderText = "Book / Clusters";
            col1.Name = "Book";
            col1.Visible = true;
            dataGridView2.Columns.Add(col1);

          


            // Set the selection background color for all the cells.
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.White;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.Black;
            dataGridView2.DefaultCellStyle.SelectionBackColor = Color.White;
            dataGridView2.DefaultCellStyle.SelectionForeColor = Color.Black;

            try
            {

                chart1.Series.Clear();
                chart1.Titles.Clear();
                chart1.Legends.Clear();

            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message.ToString());
            }
            for (int i = 0, k = 1; i < NumofClusters; i++, k++)
            {
                chart1.Series.Add("Cluster" + k);
                chart1.Series["Cluster" + k].ChartType = SeriesChartType.Column;
                // Draw chart as 3D 
                chart1.Series["Cluster" + k]["DrawingStyle"] = "Cylinder";
                chart1.Series["Cluster" + k].AxisLabel = "Name";
                chart1.Series["Cluster" + k]["PointWidth"] = "0.9";
                chart1.ChartAreas[0].AxisX.MajorGrid.Interval = 1; 
                 chart1.ChartAreas[0].AxisX.MajorGrid.IntervalOffset = 0.5;
               



                // Show data points labels
                chart1.Series["Cluster" + k].IsValueShownAsLabel = true;
                // Set data points label style            
                chart1.Series["Cluster" + k]["BarLabelStyle"] = "Center";

            }

            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();


            legend1.Name = "Legend1";
            // legend1.Title = "Book Information";


            chart1.Legends.Add(legend1);
            chart1.Legends[0].Docking = Docking.Top;






            chart1.Titles.Add(new Title("Clustering distribution", Docking.Top, new System.Drawing.Font("Arial", 13, System.Drawing.FontStyle.Bold), System.Drawing.Color.FromArgb(16, 78, 139)));



            // starting new thread
            ThreadStart job = new ThreadStart(ThreadJob);
            Thread thread = new Thread(job);
            thread.Start();

            

        }


    
        //function that sets values in result table. 
        public void setGridtabe(List<Chart> c)
        {
            int n = listBox2.Items.Count; // number of texts total;
            int num = 0; // num will count the number of different books.
            int found; // if a new book was found.
            string[] BookNames = new string[n]; // will put book names here.
            string temp; 
            string[] tableRow = new string[NumofClusters];
        
        

        

            // count how many different book there is.
            for (int i = 0; i < n; i++)
            {
                found = 1;// starting to look for new book.
                temp = c[i].getBook();
                for (int j = 0; j < n; j++)
                {
                    if (BookNames[j] == temp)
                        found = 0; // book already exists.
                    

                }// end inner for
                if (found == 1) // if book found
                    BookNames[num++] = temp;

            }// end outer for

            updateLabelText(Convert.ToString(num), label28);//  label28.Text = Convert.ToString(num);// number of books

         
          
            assoTable = new double[num][];

            // create new rows and set it
            for (int i = 0; i < num; i++)
            {


                this.Invoke((MethodInvoker)delegate { dataGridView2.Rows.Add(BookNames[i]); });  //dataGridView2.Rows.Add(BookNames[i]); // set name of book
                
                assoTable[i] = new double[NumofClusters]; // create assosiative table as well

                for(int j = 0 ; j < NumofClusters ; j++)
                {
                    tableRow[j]= getATValue(c, BookNames[i], j); // set cluster cell
                    assoTable[i][j] = Convert.ToDouble(tableRow[j]);
                }

                this.Invoke((MethodInvoker)delegate { dataGridView1.Rows.Add(tableRow); });  //dataGridView1.Rows.Add(tableRow); // set name of book

            }// end for

  
            
            
            
            /*
             * Cramer's V
             * Calculation
             */

            CramersVal = CramersV.Cramer.CramersV(assoTable);

            updateLabelText(Convert.ToString(Math.Round(CramersVal, 3)), label26); // label26.Text = Convert.ToString(Math.Round(CramersVal, 3));// Cramer's V
            
            
            
            
            
       

                // draw graph
                for (int i = 0; i < num; i++)
                {
                    for (int j = 0, k = 1; j < NumofClusters; j++,k++)
                    {
                        this.Invoke((MethodInvoker)delegate { chart1.Series["Cluster" + k].Points.AddXY(BookNames[i], getATValue(c, BookNames[i], j)); });
                      //  chart1.Series["Cluster" + k].Points.AddXY(BookNames[i], getATValue(c, BookNames[i], j));//the string is the name of the slice'the value is the number.
                        //chart1.Series["Cluster" + k].ToolTip = c[j].getNumCluster();
                        this.Invoke((MethodInvoker)delegate { chart1.Series["Cluster" + k].ToolTip = c[j].getNumCluster(); });
                        
                    }// end inner for
                }// end outer for

                





            dataGridView1.ClearSelection();
        }// end function setGridtabe()


        private void ResetDatagridview(DataGridView dgv)
        {

            while (dgv.Rows.Count > 0)
                dgv.Rows.RemoveAt(0);
            while (dgv.Columns.Count > 0)
                dgv.Columns.RemoveAt(0);
        }
 
     


        // Extract from clustering results values for the associative table
        public string getATValue(List<Chart> c,string book, int cluster)
        {
            int n = listBox2.Items.Count; // number of texts total;
            int count = 0;

            for ( int i = 0 ; i < n ; i++)
                if (c[i].getBook() == book && c[i].getCluster() == cluster)
                    count++;

            return Convert.ToString(count);
        }// end 


        /*
         *  this function divides the book into "div" parts
         *  and creates new text files from the divided book
         *  
         */
        public string[] divideText(string filename)
        {
            string[] texts = new string[div];
           
            if (!System.IO.File.Exists(filename)) // if files doesnt exists
            {
                Error[0] = 1; //put Error[0]=1 flag on - which means file not found
                return texts;
            }
            filename = filename.Replace("\\", "\\\\");// correct file path
            
            // Read in every line in specified file.
            string[] lines = System.IO.File.ReadAllLines(filename);
            int len = lines.Length;
            double temp = len / div;
            int parts = Convert.ToInt32(Math.Floor(temp));
            if (parts < 1) // cannot divide to that many parts (text isnt long enough)
            {
                Error[0] = 2; // error : cannot divide to that many parts (text isnt long enough)
                return texts;
            }// end if text not long enough

            StreamWriter fout;
            int j = 0; // reset j here cause we dont want to reset j inside the loop
            filename = Path.ChangeExtension(filename, null);// cut off extention.
            string[] name = filename.Split('\\');
            string file = name[name.Length-1]; // extract only name of book
            
            //create folder
            // Specify a "currently active folder"
            string activeDir = @"c:\ASCoTI";

            //Create a new subfolder under the current active folder
            string newPath = System.IO.Path.Combine(activeDir);

            // Create the subfolder
            System.IO.Directory.CreateDirectory(newPath);
            
            for(int i = 0; i < div; i++)
                {
                // open output file 
                try
                    {
                        fout = new StreamWriter("C:\\ASCoTI\\" + file + i + ".txt");

                    }
                catch (IOException exc)
                    {
                        Error[0] = 3;// (exc.Message + "\nError Opening Output File");
                        return texts;
                    }

                // Write the alphabet to the file. 
                try
                    {
                        for ( ; j < parts*(i+1); j++)
                            fout.WriteLine(lines[j]);
                    }
                catch (IOException exc)
                    {
                        Error[0] = 4; // (exc.Message + "File Error");
                        return texts;
                    }

                fout.Close();
                texts[i] = "C:\\ASCoTI\\" + file + i + ".txt";
                }// end main for
            
            return texts;
        }// end function divide text into parts.


        private void label1_Click(object sender, EventArgs e)
        {

        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
               cp.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED 
               return cp;
            }
        }  

        private void button2_Click(object sender, EventArgs e) // remove selected item button
        {
            int index1 = listBox1.SelectedIndex * div;
            if (listBox1.SelectedIndex != -1 && bowFromFile == 0)
            {
                for (int i = 0; i < div; i++)
                    Functions.Start().mySettings.RemoveFromListbox(index1);
                listBox1.Items.Remove(listBox1.SelectedItems[0]);
            }// end if
            else
            {
                Functions.Start().mySettings.RemoveFromListbox(index1);
                listBox1.Items.Remove(listBox1.SelectedItems[0]);
            }
        }

        private void button3_Click(object sender, EventArgs e)  // clear all docuemnts button
        {

            DialogResult result;
            result = MessageBox.Show("Are you sure you want to clear all documents?", "Clear Documents", MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
            {
                listBox1.Items.Clear();                             // gui clear.
                Functions.Start().mySettings.ClearAllDocuments();   // real listbox clear.
                Array.Clear(fileName, 0, fileName.Length);
                Array.Clear(myPath, 0, myPath.Length);
            }



            
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)  // quit button event
        {

            DialogResult result;
            result = MessageBox.Show("Quit?", "Exit Program", MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
            {
                Application.Exit();
            }

            
            
            
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

   

    private bool validateOptions()
        {

            if (brentsTolerance < 0 || brentsTolerance > 1)
            {
                MessageBox.Show("Invalid tolerance value", "Error");
                button4.Image = ((System.Drawing.Image)(Properties.Resources.start11)); // change image at click
                return false;
            }
            else if (Sigma == 0)
            {
                MessageBox.Show("Invalid Sigma value", "Error");
                button4.Image = ((System.Drawing.Image)(Properties.Resources.start11)); // change image at click
                return false;
            }
            else if (Numoftxt == 0)
            {
                MessageBox.Show("Please Insert Documents", "Error");
                button4.Image = ((System.Drawing.Image)(Properties.Resources.start11)); // change image at click
                return false;
            }
            else if (ButtonPress == "on")
                return false;
            
            return true;
        }

    private void label8_Click(object sender, EventArgs e)
    {

    }

    private void label17_Click(object sender, EventArgs e)
    {

    }

    private void button6_Click_1(object sender, EventArgs e) // save button in the option tab
    {
        brentsTolerance = System.Convert.ToDouble(textBox1.Text);        
        NumofClusters = System.Convert.ToInt32(numericUpDown1.Value);
        epsilon = System.Convert.ToDouble(textBox4.Text);
        runs = System.Convert.ToInt32(textBox3.Text);
        div = System.Convert.ToInt32(numericUpDown2.Value);
        Sigma = System.Convert.ToDouble(textBox2.Text);

        
        if (radioButton5.Checked == true)
            typeofCluster = "PAM";
        else if (radioButton6.Checked == true)
            typeofCluster = "K-means";
        else if (radioButton8.Checked == true)
            this.bowFromFile = 1;
        else if (radioButton7.Checked == true)
            this.bowFromFile = 0;
       
        if (radioButton1.Checked == true)
        {
            StopBy = 0; // stop by number of runs
            progressBar1.Step = 100 / runs;
        }
        else if (radioButton2.Checked == true)
        {
            StopBy = 1; // stop by epsilon
            progressBar1.Step = 1;
        }
        else if (radioButton3.Checked == true)
        {
            StopBy = 2; // stop by combined
            progressBar1.Step = 100 / runs;
        }

        if (radioButton8.Checked == true)
            bowFromFile = 1;
        else
            bowFromFile = 0;

        


        
    
            
            

    }
        /*
    *  Extract one vector from BoW.
    *  input: BoW matrix, number of Vector to extract.
    *  output: Extracted vector.
    */
        private double[] getVector(double[][] arr, int n)
        {
            int dim = arr.GetLength(0);
            double[] values = new double[dim];

            for (int i = 0; i < dim; i++)
                values[i] = arr[i][n];

            return values;
        } // end getVector

    private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
    {

    }

    private void tabPage3_Click(object sender, EventArgs e)
    {

    }

    private void label31_Click(object sender, EventArgs e)
    {

    }

    private void label6_Click(object sender, EventArgs e)
    {

    }

    private void label7_Click(object sender, EventArgs e)
    {

    }

    private void textBox1_TextChanged(object sender, EventArgs e)
    {

    }

    private void button7_Click(object sender, EventArgs e) // help button
    {
        string directoryString = Directory.GetCurrentDirectory() + @"\Documentation\User Manual.pdf";
     
            if (File.Exists(directoryString))
                System.Diagnostics.Process.Start(directoryString );
            else
                MessageBox.Show("File UserManual.pdf not found"); 
        

    }

    private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    private void label15_Click(object sender, EventArgs e)
    {

    }

    private void panel1_Paint(object sender, PaintEventArgs e)
    {

    }

    private void panel5_Paint(object sender, PaintEventArgs e)
    {

    }

    private void label10_Click(object sender, EventArgs e)
    {

    }

    private void numericUpDown1_ValueChanged(object sender, EventArgs e)
    {

    }

    private void radioButton5_CheckedChanged(object sender, EventArgs e)
    {

    }

    private void label9_Click(object sender, EventArgs e)
    {

    }

    private void radioButton6_CheckedChanged(object sender, EventArgs e)
    {

    }

    private void textBox3_TextChanged(object sender, EventArgs e)
    {

    }

    private void label8_Click_1(object sender, EventArgs e)
    {

    }

    private void radioButton8_CheckedChanged(object sender, EventArgs e)
    {

    }

    private void textBox2_TextChanged(object sender, EventArgs e)
    {

    }

    private void radioButton7_CheckedChanged(object sender, EventArgs e)
    {

    }

    private void label5_Click(object sender, EventArgs e)
    {

    }

    private void label4_Click(object sender, EventArgs e)
    {

    }

    private void label2_Click_1(object sender, EventArgs e)
    {

    }

    private void progressBar1_Click_1(object sender, EventArgs e)
    {

    }

    private void panel4_Paint(object sender, PaintEventArgs e)
    {

    }

    private void panel3_Paint(object sender, PaintEventArgs e)
    {

    }

    private void label30_Click(object sender, EventArgs e)
    {

    }

    private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {

    }

    private void label24_Click(object sender, EventArgs e)
    {

    }

    private void label25_Click(object sender, EventArgs e)
    {

    }

    private void label23_Click(object sender, EventArgs e)
    {

    }

    private void label26_Click(object sender, EventArgs e)
    {

    }

    private void label27_Click(object sender, EventArgs e)
    {

    }

    private void label28_Click(object sender, EventArgs e)
    {

    }

    private void label29_Click(object sender, EventArgs e)
    {

    }

    private void chart1_Click_1(object sender, EventArgs e)
    {

    }

    private void label3_Click(object sender, EventArgs e)
    {

    }

    private void label17_Click_1(object sender, EventArgs e)
    {

    }

    private void label22_Click(object sender, EventArgs e)
    {

    }

    private void label19_Click(object sender, EventArgs e)
    {

    }

    private void label20_Click(object sender, EventArgs e)
    {

    }

    private void label21_Click(object sender, EventArgs e)
    {

    }

    private void tabPage4_Click(object sender, EventArgs e)
    {

    }

    private void label16_Click(object sender, EventArgs e)
    {

    }

    private void label14_Click(object sender, EventArgs e)
    {

    }

    private void label13_Click(object sender, EventArgs e)
    {

    }

    private void label12_Click(object sender, EventArgs e)
    {

    }

    private void label18_Click(object sender, EventArgs e)
    {

    }

    private void chartBindingSource1_CurrentChanged(object sender, EventArgs e)
    {
        
    }

    private void chartBindingSource_CurrentChanged(object sender, EventArgs e)
    {

    }

    private void button8_Click(object sender, EventArgs e) // capture chart and save as jpg
    {
        int hight = tabControl1.Size.Height - tabPage3.Size.Height + label34.Size.Height ;
        int width = tabControl1.Size.Width - tabPage3.Size.Width;
        Rectangle form = this.Bounds;
        Rectangle chart = new Rectangle(width + form.Location.X + chart1.Location.X + tabControl1.Location.X, hight + form.Location.Y + chart1.Location.Y + tabControl1.Location.Y, chart1.Size.Width, chart1.Size.Height);
        
        
        Bitmap bitmap = new Bitmap(chart.Width, chart.Height); 
    // Size size is how big an area to capture
    // pointOrigin is the upper left corner of the area to capture
      
    
    {
        using (Graphics graphics = Graphics.FromImage(bitmap))
        {
           // graphics.CopyFromScreen(0, new Point(0, 0), 200,300);
            graphics.CopyFromScreen(chart.Location, Point.Empty, chart.Size);
        }
    }

    //Show a save dialog to allow the user to specify where to save the image file 
    using (SaveFileDialog dlgSave = new SaveFileDialog())
    {
        dlgSave.Title = "Save Image";
        dlgSave.Filter = "JPEG file (*.jpg)|*.jpg|All Files (*.*)|*.*";
        if (dlgSave.ShowDialog(this) == DialogResult.OK)
        {

                bitmap.Save(dlgSave.FileName); 
        }
    }



   
    }


    private void dataGridView2_Scroll(object sender, ScrollEventArgs e)
    {
        dataGridView1.FirstDisplayedScrollingRowIndex = dataGridView2.FirstDisplayedScrollingRowIndex;
        dataGridView1.HorizontalScrollingOffset = dataGridView2.HorizontalScrollingOffset;
    }




    private void button9_Click(object sender, EventArgs e) // capture associative table and save as jpg
    {

        int hight = tabControl1.Size.Height - tabPage3.Size.Height + label33.Size.Height;
        int width = tabControl1.Size.Width - tabPage3.Size.Width;
        Rectangle form = this.Bounds;
        Rectangle chart = new Rectangle(width + form.Location.X + dataGridView2.Location.X + tabControl1.Location.X, hight + form.Location.Y + dataGridView1.Location.Y + tabControl1.Location.Y, dataGridView1.Size.Width + dataGridView2.Size.Width, dataGridView1.Size.Height);


        Bitmap bitmap = new Bitmap(chart.Width, chart.Height);
        // Size size is how big an area to capture
        // pointOrigin is the upper left corner of the area to capture


        {
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                // graphics.CopyFromScreen(0, new Point(0, 0), 200,300);
                graphics.CopyFromScreen(chart.Location, Point.Empty, chart.Size);
            }
        }

        //Show a save dialog to allow the user to specify where to save the image file 
        using (SaveFileDialog dlgSave = new SaveFileDialog())
        {
            dlgSave.Title = "Save Image";
            dlgSave.Filter = "JPEG file (*.jpg)|*.jpg|All Files (*.*)|*.*";
            if (dlgSave.ShowDialog(this) == DialogResult.OK)
            {

                bitmap.Save(dlgSave.FileName);
            }
        } 




    }

    private void radioButton2_CheckedChanged(object sender, EventArgs e)
    {

    }

    private void button10_Click(object sender, EventArgs e) // save bow button
    {
        if (SaveSafe == 1) // if bow was set put into text file if not error messege
        {

            // BoW            
            BagOfWords.BagOfWords bow = new BagOfWords.BagOfWords(Functions.Start().mySettings.getFullPath());
            bow.StartBagOfWords();
            BOW = bow.GetNormalizeBowMatrix();
            SaveBow = BOW;

            string activeDir = @"c:\ASCoTI1";
            //Show a save dialog to allow the user to specify where to save the image file 
            using (SaveFileDialog dlgSave1 = new SaveFileDialog())
            {
                dlgSave1.Title = "Save File";
                dlgSave1.Filter = "Text file (*.txt)|*.txt|All Files (*.*)|*.*";
                if (dlgSave1.ShowDialog(this) == DialogResult.OK)
                {

                    activeDir = dlgSave1.FileName;
                }
            }

            StreamWriter fout;

            fout = new StreamWriter(activeDir);
            int n1 = SaveBow.GetLength(0);
         
            // Write the alphabet to the file. 
            string temp;
            for (int i = 0; i < SaveBow.GetLength(0); i++)
            {
                for (int j = 0; j < SaveBow[0].Length; j++)
                {
                    fout.Write(SaveBow[i][j]);
                    fout.Write(" ");
                }
                fout.WriteLine("");
            }
            
            fout.WriteLine("*"); // start saving paramets.
            fout.WriteLine(listBox2.Items.Count); // number of total divded items.
            fout.WriteLine(div);
            for (int i = 0; i < listBox2.Items.Count; i++)
            {
                temp = listBox2.Items[i].ToString();
              //  temp = temp.Replace("\\","\\\\");
                fout.WriteLine(temp);
            }
            fout.Write(SaveBow.GetLength(0)); // Save bow length
        fout.Close();
        }
        else
            MessageBox.Show("BOW information doesnt exists.");
    }




    
    }// end class

    public class Chart
    {       
        private string text;
        private string book;
        private int cluster;
        private string numCluster;
        private double[] data;

        public Chart(string text, int cluster, string numCluster,double[] col)
        {
            int len;
            this.text = text;
            this.cluster = cluster;
            this.numCluster = numCluster;
            data = new double[col.Length];
            this.data = col;
            len = text.Length;
            if (len != 0)
            {
                book = text;
                book = Path.ChangeExtension(text, null);// cut off extention.
                string[] split = book.Split('\\');
                book = split[split.Length - 1];
                while(Char.IsNumber(book[book.Length-1])) // only if the text has a number at the end ( it means its a part of a book or a chapter ) then remove it.
                    book = book.Remove(book.Length - 1);     
            }// end if
        }// end constructor 

        public string getText()
        {
            return text;
        }

        public int getCluster()
        {
            return cluster;
        }

        public string getNumCluster()
        {
            return numCluster;
        }


        public string getBook()
        {
            return book;
        }
        public double[] getVector()
        {
            return data;
        }

        public void setCluster(int c)
        {

            this.cluster = c;

        }

    }// end class showChart

   


}
