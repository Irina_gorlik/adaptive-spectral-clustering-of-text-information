using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScaleFactor;
using MathLib;

namespace Brent
{

    class Brent
    {
        private double ITMAX  = 100; // max itarations.
        private double CGOLD  = 0.3819660;
        private double ZEPS = 1.0e-10;
       	private int iter;
	    private double a,b,d,etemp,fu,fv,fw,fx,p,q,r,tol,tol1,tol2,u,v,w,x,xm;
        private double e=0.0;
        private int inputSize = 0;
        double[][] dataset;
        private int NumofClusters;
        List<Final_Project.Chart> c;
        /*
         *   Given a function f, and given a bracketing triplet of abscissas ax, bx, cx (such that bx is
         *   between ax and cx,and f(bx) is less than both f(ax) and f(cx)), this routine isolates
         *   the minimum to a fractional precision of about tol using Brent�s method. The abscissa of
         *   the minimum is returned as xmin, and the minimum function value is returned as brent,the
         *   returned function value.
         */
        public Brent(double ax, double bx, double cx, double tol, ref double xmix, int numofClusters, int inputsize, List<Final_Project.Chart> C)
        {
            
            this.inputSize = inputsize;
            this.NumofClusters = numofClusters;
            this.c= C;
            //  double ax, bx, cx;          
         //   ax = f(aa, dataset,clusters);
         //   bx = f(bb, dataset, clusters);
        //    cx = f(cc, dataset,clusters);





            if( ax < cx )
            {
                
                a = ax;
                b = cx;
            }
            else 
            {
                a = cx;
                b = ax;
            }
           
            
            
            this.tol = tol;
            x=w=v=bx;
            fx = f(x,c);
            fw=fv=fx;

        }// end constructor 

      
        private double SIGN(double a, double b)
        {
            if(b > 0.0)
                return Math.Abs(a);
            else 
                return -Math.Abs(a);

        }// end SIGN
        
      
        private void SHFT(ref double a, ref  double b, ref double c, ref double d)
        {
            a = b;
            b = c;
            c = d;

        }// end SHFT

        private double ftest(double x, double[][] dataset, int[] clusters)
        {
            double ret = 0;

            ret = Math.Sin(x);


            return ret;
        }

/*
        private double f1(double x, double[][] dataset, List<Final_Project.Chart> c)
        {
            double A = 0; // the simmilar part of the equation.
            double B = 0; // the dissimlar part of the equation.
            int[] markClusters = new int[inputSize]; // mark cluster that are 
            int vectorSize = dataset.GetLength(0);
            int same = -1;
            int NumofClusters = -1;

            // find out how many clusters
            for (int i = 0; i < inputSize; i++)
                if (NumofClusters < clusters[i])
                    NumofClusters = clusters[i];



            for (int k = 0; k <= NumofClusters; k++)
            {
                for (int i = 0; i < inputSize; i++)
                {
                    if (clusters[i] == k) // if same cluster
                    {
                        MathLib.Vector v1 = new MathLib.Vector(getVector(dataset, i)); // S(i)

                        for (int j = 0; j < inputSize; j++)
                        {
                            if (clusters[j] == k)
                            {

                                MathLib.Vector v2 = new MathLib.Vector(getVector(dataset, j)); // S(j)
                                MathLib.Vector v3 = new MathLib.Vector(vectorSize); // v3=v1-v2
                                v3 = v1 - v2; // si-sj
                                A += (1 - Math.Exp(-((v3.Abs() * v3.Abs()) / (2 * (x * x))))) * (1 - Math.Exp(-((v3.Abs() * v3.Abs()) / (2 * (x * x)))));     //                          
                            }

                        }// end inner for
                    }// end if
                    else
                    {
                        MathLib.Vector v4 = new MathLib.Vector(getVector(dataset, i)); // S(i)

                        for (int j = 0; j < inputSize; j++)
                        {
                            if (clusters[j] != k)
                            {

                                MathLib.Vector v5 = new MathLib.Vector(getVector(dataset, j)); // S(j)
                                MathLib.Vector v6 = new MathLib.Vector(vectorSize); // v3=v1-v2
                                v6 = v4 - v5; // si-sj
                                B += (1 - Math.Exp(-((v6.Abs() * v6.Abs()) / (2 * (x * x)))));     //                          
                            }

                        }// end inner for
                    }// end else
                }// end outer for
            }// end first for





            // double ret = A - Math.Log(B);
            return (A - Math.Log10(B));
        }// end f
 */

        /*
         *  Clustering quality function 
         */
/*
        private double f(double x, List<Final_Project.Chart> c)
        {
            double A = 0; // the simmilar part of the equation.
            double B = 0; // the dissimlar part of the equation.      
            int S_i = 0;// 0 - not set, 1 - set
            MathLib.Vector v1 = new MathLib.Vector(inputSize); 
            MathLib.Vector v2 = new MathLib.Vector(inputSize); 
            MathLib.Vector v3 = new MathLib.Vector(inputSize); 

            // Simmilar
            for (int k = 0; k < NumofClusters; k++)
            {
                for (int i = 0; i < inputSize; i++)
                {
                    if (c[i].getCluster() == k && S_i == 0)
                    {
                        v1 = new MathLib.Vector(c[i].getVector()); // S(i)
                        S_i = 1; // is set 
                        continue;
                    }// end if
                    else if (c[i].getCluster() == k)
                    {
                        v2 = new MathLib.Vector(c[i].getVector()); // S(j)
                        v3 = v1 - v2; // si-sj
                        A += (1 - Math.Exp(-((v3.Abs() * v3.Abs()) / (2 * (x * x))))) * (1 - Math.Exp(-((v3.Abs() * v3.Abs()) / (2 * (x * x)))));     //                          
                        S_i = 0; // reset 
                    }//end if else
                }// end inner for 
            }// end outer for



            // Disimmilar
            for (int i = 0; i < inputSize; i++)
            {
                v1 = new MathLib.Vector(c[i].getVector()); // D(i)
             
                for (int j = 0; j < inputSize; j++)
                {
                    if (c[j].getCluster() != c[i].getCluster())
                    {
                        v2 = new MathLib.Vector(c[j].getVector()); // D(j)
                        v3 = v1 - v2; // si-sj
                        B += (1 - Math.Exp(-((v3.Abs() * v3.Abs()) / (2 * (x * x)))));
                    }//end if else
                }// end inner for 
            }// end outer for



           // double ret = A - Math.Log(B);
            return (A-Math.Log10(B));
        }// end f
        */


        /*
      *  Clustering quality function 
      */
        private double f(double x, List<Final_Project.Chart> c)
        {
            double A = 0; // the simmilar part of the equation.
            double B = 0; // the dissimlar part of the equation.      
            MathLib.Vector v1 = new MathLib.Vector(inputSize);
            MathLib.Vector v2 = new MathLib.Vector(inputSize);
            MathLib.Vector v3 = new MathLib.Vector(inputSize);

            // Simmilar
            for (int i = 0; i < inputSize-1; i++)
            {
                v1  = new MathLib.Vector(c[i].getVector()); // S(i)
                for (int j = 0; j < inputSize; j++)
                {
                    if (c[i].getCluster() == c[j].getCluster() && (i != j))
                    {
                        v2 = new MathLib.Vector(c[j].getVector()); // S(j)
                        v3 = v1 - v2; // si-sj
                        A += (1 - Math.Exp(-((v3.Abs() * v3.Abs()) / (2 * (x * x))))) * (1 - Math.Exp(-((v3.Abs() * v3.Abs()) / (2 * (x * x)))));     //                          
                    }//end if 
                }// end inner for 
            }// end outer for



            // Disimmilar
            for (int i = 0; i < inputSize; i++)
            {
                v1 = new MathLib.Vector(c[i].getVector()); // D(i)
                for (int j = 0; j < inputSize-1; j++)
                {
                    if (c[i].getCluster() != c[j].getCluster() && (i != j))
                    {
                        v2 = new MathLib.Vector(c[j].getVector()); // D(j)
                        v3 = v1 - v2; // si-sj
                        B += (1 - Math.Exp(-((v3.Abs() * v3.Abs()) / (2 * (x * x)))));
                    }//end if else
                }// end inner for 
            }// end outer for



            // double ret = A - Math.Log(B);
            return (A - Math.Log10(B));
        }// end f

        /*
         *  Extract one vector from BoW.
         *  input: BoW matrix, number of Vector to extract.
         *  output: Extracted vector.
         */
        private double[] getVector(double[][] arr, int n)
        {
            int dim = arr.GetLength(0);
            double[] values = new double[dim];

            for (int i = 0; i < dim; i++)
                values[i] = arr[i][n];

            return values;
        } // end getVector


        public double brentMethod(ref double xmin)
        {
            
            for (iter=1;iter<=ITMAX;iter++) {
		            xm=0.5*(a+b);
		            tol2=2.0*(tol1=tol*Math.Abs(x)+ZEPS);
		            if (Math.Abs(x-xm) <= (tol2-0.5*(b-a))) {
			            xmin=x;
			            return fx;
		            }
		            if (Math.Abs(e) > tol1) {
			            r=(x-w)*(fx-fv);
			            q=(x-v)*(fx-fw);
			            p=(x-v)*q-(x-w)*r;
			            q=2.0*(q-r);
			            if (q > 0.0) p = -p;
			            q=Math.Abs(q);
			            etemp=e;
			            e=d;
			            if (Math.Abs(p) >= Math.Abs(0.5*q*etemp) || p <= q*(a-x) || p >= q*(b-x))
				            d=CGOLD*(e=(x >= xm ? a-x : b-x));
			            else {
				            d=p/q;
				            u=x+d;
				            if (u-a < tol2 || b-u < tol2)
					            d=SIGN(tol1,xm-x);
			            }
		            } else {
			            d=CGOLD*(e=(x >= xm ? a-x : b-x));
		            }
		            u=(Math.Abs(d) >= tol1 ? x+d : x+SIGN(tol1,d));
		            fu=f(u,c);
		            if (fu <= fx) {
			            if (u >= x) a=x; else b=x;
			            SHFT(ref v,ref w,ref x,ref u);
			            SHFT(ref fv,ref fw,ref fx,ref fu);
		            } else {
			            if (u < x) a=u; else b=u;
			            if (fu <= fw || w == x) {
				            v=w;
				            w=u;
				            fv=fw;
				            fw=fu;
			            } else if (fu <= fv || v == x || v == w) {
				            v=u;
				            fv=fu;
			            }
		            }
	            }
	           // nrerror("Too many iterations in BRENT");
	            xmin=x;
	            return fx;
            } // end brentMethod

    }// end class
}// end namespace