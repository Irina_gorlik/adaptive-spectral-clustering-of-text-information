using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using BagOfWords;
using Final_Project;

namespace CramersV
{

    class Cramer
    {

        /** The small deviation allowed in double comparisons. */
        public static double SMALL = 1e-6;

        

        /**
         * Computes Cramer's V for a contingency table.
         *
         * @param matrix the contingency table
         * @return Cramer's V
         */
        public static double CramersV(double[][] matrix)
        {

            int row, col, nrows, ncols, min;
            double n = 0;

            nrows = matrix.GetLength(0);
            ncols = matrix[0].Length;
            for (row = 0; row < nrows; row++)
            {
                for (col = 0; col < ncols; col++)
                {
                    n += matrix[row][col];
                }
            }
            min = nrows < ncols ? nrows - 1 : ncols - 1;
            if ((min == 0) || eq(n, 0))
                return 0;
            
            return Math.Sqrt(chiVal(matrix, false) / (n * (double)min));
        }

        

             /** Computes chi-squared statistic for a contingency table.
             *
             * @param matrix the contigency table
             * @param useYates is Yates' correction to be used?
             * @return the value of the chi-squared statistic
             */
            public static double chiVal(double[][] matrix, bool useYates) {

                int df, nrows, ncols, row, col;
                double[] rtotal, ctotal;
                double expect = 0, chival = 0, n = 0;
                bool yates = true;

                nrows = matrix.GetLength(0);
                ncols = matrix[0].Length;
                rtotal = new double[nrows];
                ctotal = new double[ncols];
                for (row = 0; row < nrows; row++) {
                    for (col = 0; col < ncols; col++) {
                        rtotal[row] += matrix[row][col];
                        ctotal[col] += matrix[row][col];
                        n += matrix[row][col];
                    }
                }
                df = (nrows - 1) * (ncols - 1);
                if ((df > 1) || (!useYates)) {
                    yates = false;
                } else if (df <= 0) {
                    return 0;
                }
                chival = 0.0;
                for (row = 0; row < nrows; row++) {
                    if (gr(rtotal[row], 0)) {
                        for (col = 0; col < ncols; col++) {
                            if (gr(ctotal[col], 0)) {
                                expect = (ctotal[col] * rtotal[row]) / n;
                                chival += chiCell(matrix[row][col], expect,yates);
                            }
                        }
                    }
                }
                return chival;
            }

      /**
      * Computes chi-value for one cell in a contingency table.
      *
      * @param freq the observed frequency in the cell
      * @param expected the expected frequency in the cell
      * @return the chi-value for that cell; 0 if the expected value is
      * too close to zero 
      */
            private static double chiCell(double freq, double expected, bool yates)
            {

                // Cell in empty row and column?
                if (smOrEq(expected, 0))
                {
                    return 0;
                }

                // Compute difference between observed and expected value
                double diff = Math.Abs(freq - expected);
                if (yates)
                {

                    // Apply Yates' correction if wanted
                    diff -= 0.5;

                    // The difference should never be negative
                    if (diff < 0)
                    {
                        diff = 0;
                    }
                }

                // Return chi-value for the cell
                return (diff * diff / expected);
            }

             /**
             * Tests if a is equal to b.
             *
             * @param a a double
             * @param b a double
             */
            public static/*@pure@*/bool eq(double a, double b) {

                return (a - b < SMALL) && (b - a < SMALL);
            }

            /**
              * Tests if a is smaller or equal to b.
              *
              * @param a a double
              * @param b a double
              */
            public static/*@pure@*/bool smOrEq(double a, double b)
            {

                return (a - b < SMALL);
            }


     /**
     * Tests if a is greater than b.
     *
     * @param a a double
     * @param b a double
     */
            public static /*@pure@*/ bool gr(double a, double b)
            {

                return (a - b > SMALL);
            }




    }// end class
}// end namespace