using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using BagOfWords;
using Final_Project;
using MathLib;
using  PAMAlgorithm;
using KdKeys.DataMining.Clustering.KMeans;
using Mapack;

namespace SpectralClustering
{
    class SpectralClustering
    {

      //  private int NumberofClusters = 3;
        private int inputSize = 0;    // size of dateset S
        private int vectorSize = 0;   // size of vectors in dateset S
        private string typeofCluster = "";
        private double[,] A;          // affinity matrix
        private double[,] D;          // diagonal matrix
        private double[,] DNormilazed;// DNormalized = D^(-0.5).
        private double[,] L;          // laplacian: L=D^(-0.5)*A*D^(-0.5).
        private double[,] X;          // k largest eigenvectors of L.
        private double[,] Y;          // normalized k largest eigenvectors.
        private double[][] S;            // input S={s1,...,sn).
        private double sigma = 0;
        private int[] PamResult;      // results from PAM algorithm.
        private int[] KResult;      // results from k-means algorithm.
        private double[,] ClusterRes;      // final clustering results.
        private int NumofClusters;
        Matrix V;

        private double[,] vectorr;

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Constructor
        /// </summary>
        ///
        public SpectralClustering(double[][] b, int num, string typeofCluster,double sigma, int NumofCluster)
        {
            S = b;
            vectorSize = b.GetLength(0); // get vector size
            inputSize = num; // size of dataset S (number of documents). 
            this.typeofCluster = typeofCluster;
            A = new double[inputSize, inputSize]; // init affinity matrix
            D = new double[inputSize, inputSize]; // init diagonal matrix
            Y = new double[inputSize, NumofClusters];
            this.sigma = sigma;
            NumofClusters = NumofCluster;
            KResult = new int[inputSize];
            V = new Matrix(inputSize, inputSize);
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public int[] StartClustering()
        {
            buildAffinity();             // 1.  build affinity matrix.
            diagonalMatrix();            // 2.1 build diganoal matrix.
            buildLmat();                    // 2.2 build L=D^(-0.5)*A*D^(-0.5).
            findEigenvectors();          // 3.  find k largest eigenvectors.
            normalizeVectors();          // 4.  normalize eigenvectors.
            if (typeofCluster == "PAM") // 5.  cluster with PAM or K-means.
            {
                PAMAlgorithm.Pam pam = new PAMAlgorithm.Pam(conArray(Y), NumofClusters); // NumberofClusters = k ,  PAMAlgorithm.Pam pam = new PAMAlgorithm.Pam(conArray(Y), inputSize);
                PamResult = pam.RunAlgorithm();
                return PamResult;              // 6. assign points from clustering results to the original dataset. ( will be done in Form1.cs in the next step. )
            }
            else if(typeofCluster == "K-means")
            {
                ClusterCollection clusters;
                clusters = KMeans.ClusterDataSet(NumofClusters, Y);
                int index = -1;
                double[] temp = new double[NumofClusters];
                for (int i = 0; i < clusters.Count; i++)
                {
                    //System.Console.Out.Write(clusters.Count);  
                   // System.Console.Out.Write(i + ") ( ");
                    for (int j = 0; j < clusters[i].Count; j++)
                    {
                        // System.Console.Out.Write(clusters[i].Count );
                        for (int k = 0; k < clusters[i][j].Length; k++) // one point (full row)
                        {
                            temp[k] = clusters[i][j][k];


                            
                        }
                        index = findCluster(conArrayD(Y), temp);
                        if (index != -1) // cluster and row were matched
                        {
                            KResult[index] = i;
                            index = -1; // reset index.
                        }
                    }
                }

                return KResult; // 6. assign points from clustering results to the original dataset. ( will be done in Form1.cs in the next step. )
      
            }

            return PamResult;    // remember to remove after k means is done

        }// end StartClustering

        private void buildAffinity()
        {
            for (int i = 0; i < inputSize; i++)
            {
                MathLib.Vector v1 = new MathLib.Vector(getVector(S,i)); // S(i)

                for (int j = 0; j < inputSize; j++)
                {
                    if (i != j) // A[i,j] = exp(-||si-sj||^2/2sigma^2).
                    {
                        MathLib.Vector v2 = new MathLib.Vector(getVector(S, j)); // S(j)
                        MathLib.Vector v3 = new MathLib.Vector(vectorSize); // v3=v1-v2
                        
                        v3 = v1 - v2; // si-sj

                        A[i, j] = Math.Exp(-((v3.Abs() * v3.Abs()) / (2 * (sigma * sigma))));     //                       
                        
                    }
                    else
                        A[i,j] = 0F;



                }// end inner for
                }// end outer for

            } // ends buildAffinity()

        private void diagonalMatrix()
        {

            for (int i = 0; i < inputSize; i++)
            {
                double sum_row = 0;
                for (int j = 0; j < inputSize; j++)
                    sum_row += A[i, j];
                D[i, i] = sum_row;
            }// end outer for

         }// end diagonalMatrix()

        private void buildLmat() // ** changed
        {

     //       DNormilazed = MatrixLibrary.Matrix.Inverse(D);          // D^-1

            DNormilazed = new double[inputSize, inputSize];

            for (int i = 0; i < inputSize; i++)   // D^(-0.5)
                   DNormilazed[i, i] = Math.Pow(D[i,i],-0.5);
           

            L = MatrixLibrary.Matrix.Multiply(A, DNormilazed); //  L = MatrixLibrary.Matrix.Multiply(DNormilazed, A); // ***changed
            L = MatrixLibrary.Matrix.Multiply(DNormilazed, L); //    L = MatrixLibrary.Matrix.Multiply(L, DNormilazed); // ***changed


        }// end buildLmat()




        private void findEigenvectors()
        {
          MatrixLibrary.Matrix matL = new MatrixLibrary.Matrix(L);  // convert L to matrix to operate on.
            MatrixLibrary.Matrix vector;
           MatrixLibrary.Matrix values;
            MatrixLibrary.Matrix.Eigen(matL, out values, out vector); // find eigenvectors
            vector = MatrixLibrary.Matrix.Inverse(vector);
            Matrix Mat = new Matrix(conArrayD(L));
            
            EigenvalueDecomposition eigen = new EigenvalueDecomposition(Mat);
            V= eigen.EigenvectorMatrix.Inverse;
            double[][] eigen_V = V.Array;
            X = new double[inputSize, NumofClusters];
            
            // choose k largest eigenvectors from V
            for (int i = 0; i < NumofClusters; i++)
            {
                for (int j = 0; j < inputSize; j++)
                {
                    X[j, i] = eigen_V[i][j]; //stack the eigenvectors in columns.

                }// end inner for

            }// end outer for

















            //test delete after use
            //create folder
            // Specify a "currently active folder"
            string activeDir = @"c:\ASCoTI1";

            //Create a new subfolder under the current active folder
            string newPath = System.IO.Path.Combine(activeDir);
            StreamWriter fout;
            // Create the subfolder
            System.IO.Directory.CreateDirectory(newPath);

      
                // open output file 
               
                    fout = new StreamWriter("C:\\ASCoTI1\\L.txt");

               
           

                // Write the alphabet to the file. 
              
                    for (int i = 0; i < inputSize; i++)
                    {
                        for (int j = 0; j < inputSize; j++)
                        {
                            fout.Write(L[i, j]);
                            fout.Write(" ");
                        }
                        fout.WriteLine("");
                    }
                
            
                fout.Close();
              
          
            














            /*   3.	Find the k largest eigenvectors of L:
             * Eigenvectors of a symmetric matrix are orthogonal, but only for distinct eigenvalues. 
             * thats why we are picking distinct largest eigenvalues.
             * The dominant or principal(largest) eigenvector of a matrix is an eigenvector corresponding to 
             * the eigenvalue of largest magnitude (for real numbers, largest absolute value) of that matrix.
             *//*
            double[] biggest = new double[NumofClusters];
            int[] index = new int[NumofClusters];
            double[,] Eigen_Values = MatrixLibrary.Matrix.conToArray(values);

            Array.Clear(biggest,0,biggest.Length-1);// reset biggest array
            
            for (int i = 0, k = 0; k < NumofClusters; k++) // find laggest k eigenvectors. (by finding largest absolute eigenvalues).
            {
                for (int j = 0; j < values.NoRows; j++) 
                {
                    if (biggest[k] < Math.Abs(Eigen_Values[j, i]) && !biggest.Contains(Math.Abs(Eigen_Values[j, i])))
                    {
                        
                        biggest[k] = Math.Abs(Eigen_Values[j, i]);
                        
                        index[k] = j; // save index for later.
                    }// end if
                    
                }// end inner for
                Eigen_Values[index[k], i] = 0; // flag selected eigen value.
            }// end outer for

            double[,] X_temp = MatrixLibrary.Matrix.conToArray(vector);              // convert matrix vector back to array.
            
            X = new double[inputSize,NumofClusters];

            for (int i = 0; i < NumofClusters; i++)
            {
                for (int j = 0; j < inputSize; j++)
                {
                    X[j, i] = X_temp[index[i], j]; //stack the eigenvectors in columns.

                }// end inner for

            }// end outer for
*/
        }// end findEigenvectors()
        
        /*
         *  4. Normalize eigenvectors.
         */
        private void normalizeVectors()
        {
            int a=X.GetLength(0);
            int b = X.GetLength(1);
            double[] sum_row = new double[X.GetLength(0)];
            for (int i = 0; i < (X.GetLength(0)); i++)
            {
                for (int j = 0; j < (X.GetLength(1)); j++)
                    sum_row[i] += X[i, j] * X[i, j];
                sum_row[i] = Math.Sqrt(sum_row[i]);
            }// end outer for
            Y = new double[inputSize, NumofClusters];
            for (int i = 0; i < X.GetLength(0); i++)
                for (int j = 0; j < X.GetLength(1); j++)
                    Y[i, j] = X[i, j] / sum_row[i];
            
        }// end normalizeVectors()


        private void assignPoints(int[] res)
        {
            for (int i = 0; i < NumofClusters; i++)
            {
                for (int j = 0; j < inputSize; j++)
                {


                }// end inner for

            }// end outer for



        }// end assignPoints()





        
        
        
        
        
        
        /*
         *  Extract one vector from BoW.
         *  input: BoW matrix, number of Vector to extract.
         *  output: Extracted vector.
         */
        private double[] getVector(double[][] arr, int n)
        {
            int dim = arr.GetLength(0);
            double[] values = new double[dim];

            for (int i = 0; i < dim; i++)
                values[i] = arr[i][n];

            return values;
        } // end getVector

        private float[][] conArray(double[,] new_arr)
        {
            double[,] arr = MatrixLibrary.Matrix.Transpose(new_arr);
            float[][] mat= new float[arr.GetLongLength(0)][];
            int maxi = arr.GetLength(0);
            int maxj = arr.GetLength(1);

            for (int i = 0; i < maxi; i++)
            {
                mat[i] = new float[arr.GetLength(1)];
                for (int j=0; j < maxj; j++)
                {
                    mat[i][j] =(float) arr[i, j];

                }// end inner for
            }// end outer for
                


            return mat;
        }// end conArray

        private double[][] conArrayD(double[,] arr)
        {

            double[][] mat= new double[arr.GetLongLength(0)][];
            int maxi = arr.GetLength(0);
            int maxj = arr.GetLength(1);

            for (int i = 0; i < maxi; i++)
            {
                mat[i] = new double[arr.GetLength(1)];
                for (int j=0; j < maxj; j++)
                {
                    mat[i][j] = arr[i, j];

                }// end inner for
            }// end outer for
                


            return mat;
        }// end conArray

        private int findCluster(double[][] mat, double[] x)
        {
            int index = -1;
            int found = 0;
            int maxi = mat.GetLength(0);
            int maxj = mat[0].Length;

            for (int i = 0; i < maxi; i++)
            {
                found = 0; // reset
                for (int j = 0; j < maxj; j++)
                {
                    if (mat[i][j] == x[j])
                        found++;


                }// end inner for
                if (found == NumofClusters) // row found ( text found ) 
                {
                    for (int j = 0; j < NumofClusters; j++)
                        mat[i][j] = -1; // reset the found object
                    return i; // return the found index;


                }
            }// end outer for



            return -1;
        }// end conArray
        
        

    } // end SpectralClustering class


} // end namespace SpectralClustering
