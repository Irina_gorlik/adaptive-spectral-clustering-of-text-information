using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using BagOfWords;
using Final_Project;
/*
 * The vector is stored as an array within the class. 
 * The constructor can either set this array directly, or initialises it to a particular size.
 */


namespace MathLib
{
    public class Vector
    {
        public double[] vectorArray;
        public int Dimension;

        public Vector(double[] Values)
        {
            vectorArray = Values;
            Dimension = vectorArray.GetLength(0);
        }

        public Vector(int dim)
        {
            vectorArray = new double[dim];
            Dimension = dim;
            Zero();
        }

        /*
         * The Zero() function sets the entire vector to 0.
         */

        public void Zero()
        {
            for(int i = 0; i < Dimension; i++)
            {
                vectorArray[i] = 0;
            }
        }

        /*
         * 
         * The Abs() function calculates the absolute value of the vector,
         * which is the square root of the sum of the squares of the components of the vector.
         * 
         */
        public double Abs()
        {
            double abs = 0;

            for (int i = 0; i < Dimension; i++)
            {
                abs += vectorArray[i] * vectorArray[i];
            }
            return Math.Sqrt(abs);
        }

        /*
         * The Dot() function returns the dot product of the vector,with another vector. 
         * The dot product is the sum of the products of each component in the vectors.
         */

        public double Dot(Vector vector)
        {
            if (vector.Dimension != Dimension)
            {
                return 0;
            }
            double dotProd = 0;
            for (int i = 0; i < Dimension; i++)
            {
                dotProd += vectorArray[i] * vector.vectorArray[i];
            }
            return dotProd;
        }

        /*
         * The Scale() function scales the vector by the scaling factor supplied.
         */
    /*
        public Vector Scale(double factor)
        {
            double[] values = new double[Dimension];

            for (int i = 0; i < Dimension; i++)
            {
                values[i] = vectorArray[i] * factor;
            }
            return new Vector(values);
        }
   */
        public override string ToString()
        {
            string str = "";
            for (int i = 0; i < Dimension; i++)
            {
                if (i != 0)
                {
                    str += ", ";
                }
                str += vectorArray[i];
            }
            return "(" + str + ")";
        }
        /*
         * The + and - operators are overloaded to make it easy to add and subtract vectors.
         */
        public static Vector operator +(Vector left, Vector right)
        {
            if (left.Dimension != right.Dimension)
            {
                return left;
            }
            double[] values = new double[left.Dimension];

            for (int i = 0; i < left.Dimension; i++)
            {
                values[i] = left.vectorArray[i] + right.vectorArray[i];
            }
            return (new Vector(values));
        }

        public static Vector operator -(Vector left, Vector right)
        {
            if (left.Dimension != right.Dimension)
            {
                return left;
            }
            double[] values = new double[left.Dimension];

            for (int i = 0; i < left.Dimension; i++)
            {
                values[i] = left.vectorArray[i] - right.vectorArray[i];
            }
            return (new Vector(values));
        }

    }
}
