using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace PAMAlgorithm
{


    class Pam
    {
        public int RowsNum;
        public int ColomnNum;
        private int m_nk;
        private float[][] m_aafDistMatrix;


        public int[] clusterdObjects;   // return object
        int[] Mediods;        
        float minDist = 32000;

        public Pam(int RowsNum, int ColumnNum)
        {
            this.ColomnNum = ColumnNum;
            this.RowsNum = RowsNum;
        }

        public Pam(float[][] aafBow, int k)
            : this(aafBow[0].Length, aafBow.Length) // befor transpose
        {
            m_nk = k;
            m_aafDistMatrix = CalculateDist(TransposeMatrix(aafBow));
        }

        private float[][] TransposeMatrix(float[][] aafBow)
        {

            int row;
            int col;

            row = aafBow[0].Length;
            col = aafBow.Length;

            float[][] newA = new float[row][];
            for (int i = 0; i < row; i++)
            {
                newA[i] = new float[col];
                for (int j = 0; j < col; j++)
                {
                    newA[i][j] = aafBow[j][i];
                }

            }

            return newA;
        }

        public int[] RunAlgorithm()
        {
            PAM(m_aafDistMatrix, m_nk);
            return (clusterdObjects);
        }

        public bool isBelongToMediods(int clusterNum, int objIndex)
        {
            for (int k = 0; k < clusterNum; k++)	//check if belong to mediods
            {
                if (Mediods[k] == objIndex)		//if belong to mediods
                {
                    return true;
                }
            }
            return false;
        } //isBelongToMediods

        public void costrurctMedoids(float[][] matrix, int MidIndex)
        {
            float TempDist = 0;
            float smallest;
            for (int i = 0; i < RowsNum; i++)
            {
                for (int j = 0; j < RowsNum; j++)
                {
                    if (MidIndex == 0)									//for first mediod    
                    {
                        TempDist += matrix[i][j];
                    }
                    else
                    {
                        smallest = matrix[i][j];				//find smallest distance from object j to: searched mediode(i), other mediods
                        for (int k = 0; k < MidIndex; k++)
                        {
                            if (smallest > matrix[Mediods[k]][j])
                            {
                                smallest = matrix[Mediods[k]][j];
                            }
                        }
                        TempDist += smallest;							//add the smallest distance to temp distance
                    }
                }
                if (TempDist < minDist)								//if the distance decrese with this mediod
                {
                    minDist = TempDist;
                    Mediods[MidIndex] = i;							//add this mediod to mediods array 
                }
                TempDist = 0;
            }
            //	cout<<"With "<< MidIndex+1<<" mediods the cost is: "<<minDist<<endl;

        } //costrurctMedoids


        public void swapCoordes(float[][] matrix, int clusterNum)
        {
            int mediodIndex;
            float TempDist, smallest;
            for (int i = 0; i < clusterNum; i++)		//run on all mediods, and try to swap them
            {
                mediodIndex = Mediods[i];			//save the mediod that we try to swap
                for (int j = 0; j < RowsNum; j++)			//run on objects
                {
                    if (!isBelongToMediods(clusterNum, j))
                    {												//get here with object that are not mediods
                        Mediods[i] = j;								//make the swap;
                        TempDist = 0;

                        for (int s = 0; s < RowsNum; s++)						//calcute the new distance with the new mediod
                        {
                            smallest = matrix[Mediods[i]][s];

                            for (int k = 0; k < clusterNum; k++)
                            {
                                if (smallest > matrix[Mediods[k]][s])
                                {
                                    smallest = matrix[Mediods[k]][s];
                                }
                            }
                            TempDist += smallest;							//add the smallest distance to temp distance
                        }
                        if (TempDist < minDist)							//if the distance decrese with this mediod
                        {
                            minDist = TempDist;
                            //	cout<<"**********************************************"<<endl;
                            //	cout<<"* the cost is: "<<TempDist<<" Mediods="<<mediodIndex<<" switchd by obj="<<j<<" *"<<endl;
                            //	cout<<"**********************************************"<<endl;

                            mediodIndex = j;							//save the new madiod that improve the distance
                        }
                        /*			else//just for printing
                                    {
                                    Mediods[i]=mediodIndex;//just for printing			
                                    cout<<"the cost is: "<<TempDist<<" Mediods="<<Mediods[i]<<" not switchd by obj="<<j<<endl;
                                    }*/
                        TempDist = 0;
                    }
                }
                Mediods[i] = mediodIndex;								//update the mediod
                /*									cout<<endl<<"mediods indexes (object number):";
                    for(int h=0;h<clusterNum;h++)	cout<<Mediods[h]<<" ";
                                                    cout<<endl;*/
            }
        } //swapCoordes

        public void buildClusterArray(float[][] matrix, int clusterNum)
        {
            float smallest;
            for (int j = 0; j < RowsNum; j++)
            {
                smallest = matrix[Mediods[0]][j];
                clusterdObjects[j] = 0;
                for (int k = 1; k < clusterNum; k++)
                {
                    if (smallest > matrix[Mediods[k]][j])
                    {
                        smallest = matrix[Mediods[k]][j];
                        clusterdObjects[j] = k;
                    }
                }
            }
        } //buildClusterArray


        public void PAM(float[][] matrix, int clusterNum)
        {
            float lastDist;
            Mediods = new int[clusterNum];			//hold Medioids indexes
            clusterdObjects = new int[RowsNum];

            for (int i = 0; i < clusterNum; i++)
            {
                costrurctMedoids(matrix, i);					//find the mediods
            }

            /*	buildClusterArray(matrix, clusterNum);	//for printing

                                            cout<<endl<<"before swap:"<<endl;	
                                            cout<<"belong to cluster (object number):";
                for(i=0;i<RowsNum;i++)			cout<<clusterdObjects[i]<<" ";
                                            cout<<endl<<"mediods indexes (object number):";
                for(i=0;i<clusterNum;i++)	cout<<Mediods[i]<<" ";*/

            do		//do  until no swap accurd
            {
                //	cout<<endl<<"-----------------------------------------------------------------"<<endl<<"swap:"<<endl;
                lastDist = minDist;
                swapCoordes(matrix, clusterNum);
            }
            while (minDist < lastDist);

            buildClusterArray(matrix, clusterNum);
            /*
                                            cout<<"after swap:"<<endl;	
                                            cout<<"belong to cluster (object number):";
                for(i=0;i<RowsNum;i++)			cout<<clusterdObjects[i]<<" ";
                                            cout<<endl<<"mediods indexes (object number):";
                for(i=0;i<clusterNum;i++)	cout<<Mediods[i]<<" ";*/
        } //PAM()

        public float[][] CalculateDist(float[][] matrix)
        {
            float[][] mat = new float[RowsNum][];
            for (int k = 0; k < RowsNum; k++)
            {
                mat[k] = new float[RowsNum];
            }

            for (int i = 0; i < RowsNum; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    mat[i][j] = 0;
                    if (i != j)
                    {
                        for (int l = 0; l < ColomnNum; l++)
                        {
                            mat[i][j] += (matrix[i][l] - matrix[j][l]) * (matrix[i][l] - matrix[j][l]);
                        }
                        mat[i][j] = (float)Math.Sqrt(mat[i][j]);
                        mat[j][i] = mat[i][j];/*********************************/
                    }
                }
            }
            return (mat);
        }


    } //class PAM
} //name space
