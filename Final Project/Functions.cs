using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Final_Project
{
    class Functions
    {

        private static Functions start = null;
        public Settings mySettings;
         
        private Functions()
        {
            mySettings = new Settings();
        }



        public static Functions Start()
        {
            if (null == start)
            {
                start = new Functions();
            }
            return start;
        }

        public class Settings
        {
     
            public ListBox initialListbox;
            private string runningDirectory;

            public Settings()
            {
                initialListbox = new ListBox();
                runningDirectory = System.IO.Directory.GetCurrentDirectory();
            }

            public string getRunningDirectory()
            {
                return runningDirectory;
            }

            public ListBox getFullPath()
            {
                return initialListbox;
            }
            public void AddToListbox(string myFullPath)
            {
                initialListbox.Items.Add(myFullPath);
            }
            
            public string getItemfromList(int Index)
            {
               return initialListbox.Items[Index].ToString();
            }

            public void RemoveFromListbox(int Index)
            {
                initialListbox.Items.RemoveAt(Index);
            }

            public void ClearAllDocuments()
            {
                initialListbox.Items.Clear();
            }
        


        }// end class settings

    }// end class

}// end namespace Funtcions