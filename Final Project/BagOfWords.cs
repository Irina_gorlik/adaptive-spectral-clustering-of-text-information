using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using StopWordsHandler;
using Final_Project;

namespace BagOfWords
{
    class BagOfWords
    {
        private ListBox     listBoxFileList=null;
        private string[]    asFilesPath;
        private int[][]     m_aanBow;                        // Bow matrix, not normalized
        private double[][]   m_aafNormalizedBow;              // Bow matrix, NORMALIZED
        private int[]       m_anFrequencyOfWordsInCorpus;    // number of word i in all documents
        private int[]       m_anDocumentsLength;             // number of words in document j  
        private float[]     afStandardDeviation;
        private int[][]     n_aanBow;                        // new bow matrix that is cut down to 30 per col

        
        

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="listbox2"></param>
        public BagOfWords(ListBox listbox2)
        {
            // get the input files 
            listBoxFileList = listbox2;
            CorrectFilesPath();

            // init varables      
            m_anDocumentsLength     =  new int [listbox2.Items.Count];
   //         m_bCalcBowAgain         = true;
            
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Functioon name: StartBagOfWords
        /// Function Desccriptor: this function convert all the doucment (fileList) into matrix of Bag Of Words
        /// </summary>
        public void StartBagOfWords()
        {
//            if (true == m_bCalcBowAgain)
//            {
                ConvertDocumentsIntoMatrix();
  //              m_bCalcBowAgain = false;
 //           }
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Function Name: ConvertDocumentsIntoMatrix
        /// Function Desccriptor: The method take the array of file names, and convert these documents into matrix of BOW.
        /// </summary>
        /// <param name="fileName"></param>
        /// 
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private void ConvertDocumentsIntoMatrix()
        {
            Dictionary<string, int[]> vector = new Dictionary<string, int[]>();
            int[] array;
            StopWordsHandler.StopWordsHandler stopWords = StopWordsHandler.StopWordsHandler.StopWordsStart();   // Stop words
           
            // for each Document
            for (int d = 0; d < asFilesPath.Length; d++)
            {

                // check if file exsist
                if (!System.IO.File.Exists(asFilesPath[d]))
                {
                    return;
                }

                // array of BOW
                array = new int[asFilesPath.Length];


                // Read in every line in specified file.
                string[] lines = System.IO.File.ReadAllLines(asFilesPath[d]);
                int counter=0;
                // for each line
                for (int i = 0; i < lines.Length; i++)
                {
                    
                    // remove ---- pages marks from text.
                    while (lines[i].Equals("-"))
                        if ((++counter) > 5)
                            continue;
                        
                    
                    
                    // split words and create the vector
                    string[] words = lines[i].Split(' ');

                    foreach (string word in words)
                    {

                        // convert into lower case
                        string tempWord = word.ToLower();

                        if (tempWord.Length < 4)
                        {
                            continue;
                        }

                        // remove marks from word
                        RemoveMarksFromWord(ref tempWord);  //remove ; , . ()

                        // if the word dis english word
                     //   if (false == IsNotCorrectWord(tempWord))
                     //   {
                     //       continue;
                     //   }

                        // check if stop word
                        if (true == StopWordsHandler.StopWordsHandler.IsStopWord(tempWord))
                        {
                            continue;
                        }

                        // check if not already exist
                        if (false == vector.ContainsKey(tempWord))
                        {
                            array[d] = 1;
                            vector.Add(tempWord, new int[asFilesPath.Length]);
                            vector[tempWord].SetValue(1, d);     
                       
                            // amound size of doucments
                            m_anDocumentsLength[d]++;
                        }
                        else
                        {
                            int value = vector[tempWord][d];
                            value++;
                            vector[tempWord].SetValue(value, d);

                            // amound size of doucments
                            m_anDocumentsLength[d]++;
                        }
                    } // foreach

                } // for lines

               
            }// for

            // update process bar bow
          //  ProcessForm.Instance().IncrementProgressBarBow();

            // copy Dictionary into array
            m_aanBow = new int[vector.Count][];
            vector.Values.CopyTo(m_aanBow, 0);

            // calc Frequency Of Words In Corpus
            m_anFrequencyOfWordsInCorpus = new int[vector.Count];
            for (int i = 0; i < vector.Count; i++)
            {
                m_anFrequencyOfWordsInCorpus[i] = m_aanBow[i].Sum();                
            }

            // create BOW normalized
            m_aafNormalizedBow = new double[m_aanBow.Length][];

            for (int nWord = 0; nWord < m_aanBow.Length; nWord++)
            {
                // initlize the number of colums to this row
                m_aafNormalizedBow[nWord] = new double[m_aanBow[nWord].Length];

                // insert the normalized data
                for (int nDocument = 0; nDocument < m_aanBow[nWord].Length; nDocument++)
                {
                    m_aafNormalizedBow[nWord][nDocument] = (double)(m_aanBow[nWord][nDocument]) / (double)m_anDocumentsLength[nDocument];
                }

            }

            
            
        } // Start
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Function Name: IsNotCorrectWord
        /// Function Desccriptor: The method return false if the word contains numbers or symbols
        /// </summary>
        /// <param name="fileName"></param>
        private bool IsNotCorrectWord(string word)
        {
            for (int i = 0; i < word.Length; i++)
            {
                if (!(((word[i] >= 'a') && (word[i] <= 'z')) ||
                      ((word[i] >= 'A') && (word[i] <= 'Z'))))
                {
                    return false;
                }
                //if ((word[i] >= '0') && (word[i] <= '9'))
                //{
                //    return false;
                //}

                //if ((word[i] == '~') || (word[i] == '!') || (word[i] == '@') ||
                //    (word[i] == '#') || (word[i] == '$') || (word[i] == '%') ||
                //    (word[i] == '^') || (word[i] == '&') || (word[i] == '*'))
                //{
                //    return false;
                //}
            }

            return true;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Function Name:        RemoveMarksFromWord
        /// Function Desccriptor: Rmove: ( ) , : . from the word.
        /// </summary>
        /// <param name="fileName"></param>
        private  void RemoveMarksFromWord(ref string word)
        {

                  

            // remove prefix
            if (String.Equals(word[0], "("))
            {
                word.Remove(0, 1);
            } // if

            // remove sofix
            if (String.Equals(word[word.Length - 1], ')') ||
                String.Equals(word[word.Length - 1], '.') ||
                String.Equals(word[word.Length - 1], ':') ||
                String.Equals(word[word.Length - 1], '?') ||
                String.Equals(word[word.Length - 1], '!'))
            {
                word = word.Remove((word.Length - 1), 1);
            } // if

        } // RemoveMarksFromWord
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Function Name:          CorrectFilesPath
        /// Function Description:   Convert the files path in listBox into c# file path (\ -> \\)
        /// </summary>
        private void CorrectFilesPath()
        {
            try
            {
                asFilesPath = new string [listBoxFileList.Items.Count];

            }
            catch (System.NullReferenceException) { }
            
            
            
            for (int i = 0; i < listBoxFileList.Items.Count; i++)
            {
                asFilesPath[i] = (string) listBoxFileList.Items[i];
                asFilesPath[i] = asFilesPath[i].Replace("\\", "\\\\");
            } // for

        } // CorrectFilesPath
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// GetBowMatrix
        /// Function return the matrix with Bag Of Words
        /// </summary>
        /// <returns></returns>
        public int[][] GetBowMatrix()
        {
            return m_aanBow;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


         ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// GetBowMatrix(double)
        /// Function return the matrix with Bag Of Words in double
        /// </summary>
        /// <returns></returns>
         // create BOW doublele
        public double[][] GetBowMatrixDouble()
        {
            int[][] bowOrig = GetBowMatrix();
            double[][] bowRet = new double[bowOrig.Length][];
            for (int i = 0; i < bowOrig.Length; i++)
            {
                // initlize the number of colums to this row
                bowRet[i] = new double[bowOrig[i].Length];

                // insert the data
                for (int j = 0; j < bowOrig[i].Length; j++)
                {
                    bowRet[i][j] = (double)(bowOrig[i][j]);
                }

            }
            return bowRet;
        }// end function
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Return matrix of bow (normalized)
        /// </summary>
        /// <returns></returns>
        public double[][] GetNormalizeBowMatrix()
        {
            return m_aafNormalizedBow;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// This Function return array of lenghs
        /// Every cell in the array contain length of Document i
        /// </summary>
        /// <returns></returns>
        public int[] GetDocumentsLength()
        {
            return m_anDocumentsLength;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// This Function return array of Frequency
        /// Every cell in the array contain frequency of word in the whole Curpus.
        /// </summary>
        /// <returns></returns>
        public int[] GetFrequencyOfWordsInCorpus()
        {
            return m_anFrequencyOfWordsInCorpus;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Calc standard devation to each word in the space of the documents
        /// </summary>
        private void CalcStandardDeviation()
        {
           
            float fExpectation = 0;


            // allocate
            afStandardDeviation = new float[m_aanBow.Length];

            // calc Standard Deviation
            for (int nWords = 0; nWords < m_aanBow.Length; nWords++)
            {
                fExpectation = (m_anFrequencyOfWordsInCorpus[nWords] / m_aanBow[0].Length);


                // calc numerator 
                for (int nDocument = 0; nDocument < listBoxFileList.Items.Count; nDocument++)
                {
                    afStandardDeviation[nWords] += ((m_aanBow[nWords][nDocument] - fExpectation) *
                                                    (m_aanBow[nWords][nDocument] - fExpectation));
                }

                // calc whole formula
                afStandardDeviation[nWords] /= listBoxFileList.Items.Count;
                afStandardDeviation[nWords] = (float)Math.Sqrt((double)afStandardDeviation[nWords]);

                
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Function return array (size of words)
        /// Each cell contain the standart devation of this word in all documents
        /// </summary>
        /// <returns></returns>
        public float[] getStandardDeviationForAllWords()
        {
            return afStandardDeviation;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Return number of all the words in all the documents
        /// </summary>
        /// <returns></returns>
        public int GetCorpusLength()
        {
            return (m_anFrequencyOfWordsInCorpus.Sum());
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public void SaveBowIntoFile()
        {
            System.IO.TextWriter tw = new System.IO.StreamWriter("1.txt");
            for (int i = 0; i < m_aanBow.Length; i++)
            {
                for (int j = 0; j < m_aanBow[i].Length; j++)
                {
                    tw.Write("{0, -4}", m_aanBow[i][j]);
                }
                tw.WriteLine("\n");
            }
            tw.Close();
            
        }





    } // BagOfWords


} // namespace BagOfWords
