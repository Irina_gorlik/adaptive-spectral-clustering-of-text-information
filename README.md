In this project we implement clustering of text information using the spectral clustering approach.
The project is based on the NGW (Ng, Michael and Weiss) spectral clustering algorithm. 
By using Brents method to modify the NGW algorithm, we will obtain a better clustering results.
